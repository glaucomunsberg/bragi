JFLAGS = -g
JC = javac
.SUFFIXES: .java .class
.java.class: ; $(JC) $(JFLAGS) $*.java

default: 
	javac -cp source/libraries/libApache.jar:source/libraries/libHttp.jar:source/libraries/libJson.jar source/client/*.java source/services/*.java source/Principal.java
	
clean:
	$(RM) *.class
