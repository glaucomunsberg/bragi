#####################################################################################################################
#                                  Bragi - Trabalho 2 de Redes 
#####################################################################################################################
#
#   1. Introduçao
#   2. Implementação
#	  2.1. PCSMJ
#	  2.2. Classes
#   3. Execução
#   4. Resultados e Conclusão
#   5. Bibliografia
#
#   @autor Glauco Roberto M. dos Santos 
#   @autor André Guimarães Peil
#   @autor Aline Rodrigues Tonini
#   @version 2.0
#
#####################################################################################################################
#   1. Introduçao
#####################################################################################################################
#
#       O projeto Bragi 2.0 se propõem a realizar o trabalho de Redes de Computadores, idealizado pelo Prof. Mauricio Pilla.
#  	O desenvolvimento tem como objetivo a criação um protocolo seguro de tranferencia de arquivos com usuários selecionados,
#	que seja possível
#           ✓ Obtenha uma lista de arquivos
#           ✓ Autênticar o através da utilização de SSL e certificação
#           ✓ Pedir uma lista de arquivos
#           ✓ Transferir um arquivo entre dois usuários
#	
#           
#####################################################################################################################
#   2. Implementação
#####################################################################################################################
#
#       Na implementação do Projeto Bragi optou-se por utilizar a linguagem Java e atender os paradigmas da
#   orientação a objeto. Utilizando o Bitbucket como versionador e forma de manter o código conciso e para fácil acesso aos
#    integrantes do grupo.
#
#   1.1 PCSMJ - Protocolo de Comunicação Seguro Movido a JSON
#       Protocolo proposto em aula inicialmente, possuia 13 comandos para seu funcionamento, após a inmplementação
#	da segurança na versão 2.0 foram inseridos dois novos comandos e o nome alterado para PCSMJ.
#	Segue abaixo os comandos:
#	agent-list :
#		O comando pede para o servidor intermediário enviar a lista de peers que estão conectados a ele. Ele é 
#		composto pelas partes: Protocolo que expecifica o nome, command que específica o nome do comando, o sender que é #		quem envia e #receptor que é o ip do servidor intermediário. 
#		
#		Exemplo:
#		{protocol:”pcsmj”, command:”agent-list”, sender:”<IP>”, receptor:”<IP>”}
#
#	agent-list-back:
#		O comando pelo qual o servidor intermediário retorna a lista de outros peers disponível para que o peer
#		 pedinte possa se conectar. O comando é composto por protocol, command que é o nome do comando, back que 
#		 é o retorno de um array de ips,  quem está enviando e quem está recebendo.
#		Exemplo:
#		{protocol:”pcsmj”, command:”agent-list-back”, status:”<CODIGO>”, back:”[<IP1>,<IP2>,<IPn>]”,
#		sender:”<IP>”,receptor:”<IP>”}
#
#		Codigo de Status:  200, 400, 401,501
#		
#   	ping:
#		Comando para verificar se o peer ainda está ativo, pois pode ele estar desativado, ou seja, já não esteja
#		 conectado ou online. Composto por protocol, command que é o nome do comando, quem envia e para quem é (receptor)
#		Exemplo:
#        	{protocol:”pcsmj”, command:”ping”, sender:”<IP>”,receptor:”<IP>”}
#        
#    	pong:
#		Comando pelo qual o peer destinatário retorna dizendo que ele está escutando a rede e apto para receber uma
#		conexão e pedido de rede.
#
#		Exemplo:
#        	{protocol:”pcsmj”, command:”pong”,status:”<CODIGO>”,  sender:”<IP>”,receptor:”<IP>”}
#		Codigo de Status: 100
#
#    	authenticate: (Alterado na Versão 2.0)
#		Tentativa de autenticação do peer pedinte com outro peer destinarário. Esse é o processo de autenticação entre
#		dois peers. Composto por protocol, command, nicknamer, receptor e public_key. Esse comando foi alterado
#       na versão 2.0 do protocolo, onde agora não temos mais "passport" e sim o "nickname" e a "public_key". 
#		na rede.
#		
#		Exemplo:
#		{protocol:”pcsmj”, command:”authenticate”, nick_name:<string>, public_key:<string>, ”sender:”<IP>”, receptor:”<IP>”}
#
#    	authenticate-back: 
#		Retorno pelo qual o peer destinatário diz ao peer pedinte se ele autorizou ou não a conexão com o peer. Isso é
#		importante para saber se a chave é válida na troca. Sendo a resposta true ou false. Caso alguma string mal
#		formada o servidor intermediario responde para o peer com um authenticate-back com o status 400. Caso alguma
#		requisicao de comando nao implementado o servidor intermediario responde para o peer com um authenticate-back
#	 	com o status 501.
#	
#		Exemplo:
#		{“protocol”:”pcsmj”, “command”:”authenticate-back”, ”status:”<CODIGO>”, ”sender:”<IP>”, receptor:”<IP>”}
#		Codigo de Status:200,203,400,501
#
#    	archive-list:
#       Pedido para o peer destinatário para liberar a lista de arquivo que ele possúi.
#        	
#       Exemplo:
#       {“protocol”:”pcsmj”, ”command”:”archive-list”, “sender”:”<IP>”,”receptor”:”<IP>”}
#
#    	archive-list-back:
#		Comando pelo qual o peer destinatário retorna a lista de arquivos que ele possui.
#		
#		Exemplo:
#		{"protocol":"pcsmj", "status":200, "receptor":"200.196.154.1","command":"archive-list-back",
#		"back":[{"id":3,"name":"valdirene",”size”:”12”}, {"id":3,"name":"valdirene",”size”:”200”},
#		 {"id":3,"name":"valdirene",”size”:”122”}], “sender”=”200.192.154.1”}
#
#		Obs.: O tamanho do arquivo deve ser em KB ou seja Kilobytes ( 1KB = 1024Bytes)
#		Codigo de Status:200, 400,401,501
#
#    	archive-request:
#		Aqui o peer pedinte pede para o peer destinatário um arquivo específico dá lista que ele enviou no comando
#		 archive-list-back.
#        	
#       Exemplo
#		{protocol:”pcsmj”, command:”archive-request”, id:”<ID>” sender:”<IP>”,receptor:”<IP>”}
#
#    	archive-request-back: (Alterado na Versão 2.0)
#		Retorna se o peer destinatário do archive-request  pode enviar o arquivo e informações sobre o mesmo.
#       Na versão 2.0 do protocolo retorna o https_address do arquivo, não mais o http.
#		Exemplo:
#        {protocol:”pcsmj”, command:”archive-request-back”,status:”<CODIGO>”, id:”<ID>”, https_address:”<STRING>”,
#		 size:”200”, md5:”<STRING>”, sender:”<IP>”, receptor:”<IP>”}	
#
#		Obs.: O tamanho do arquivo deve ser em KB ou seja Kilobytes ( 1KB = 1024Bytes). 
#		Codigo de Status: 302, 400,401,404,408,501
#
#     	end-connection:
# 		Modo pelo qual termina a conexão,  ou seja,  notifica aos demais peers que ele deve ser removida da lista. 
#		Exemplo:
#		{protocol:”pcsmj”, command:”end-connection”, sender:”<IP>”, receptor:”<IP>”}
#
#		certify (Criado na Versão 2.0)
#       Modo pelo qual se comunica com o servidor para se obter o certificado que atesta a autenticidade do usuário 
#       segundo a autoridade certificadora.
#       
#        Exemplo:
#       {protocol:”pcsmj”, command:”certify”,  nick_name:”<STRING>”, public_key:”<STRING>”, sender:”<IP>”, receptor:”<IP>”}
#
#		Certify-back (Criado na versão 2.0)
#		Análogo ao authenticate-back e ao archive-request-back, em relação ao primeiro retorna para o peer se ele faz
#       ou não parte do grupo. A analogia com o segundo está no momento que, caso o nick_name e chave sejam válidas,
#       retorna a string com o certificado para a comunicação. Baixar o certificado é opcional via HTTPS 
#		
#		Exemplo:
#		{protocol:”pcsmj”, command:”certify-back”,  status:”<CODIGO>”, nick_name:”<STRING>”, certify_address:”<STRING>”, sender:”<IP>”, receptor:”<IP>”}
#
#		Código de Status:
#   	 200,203,400,401,408
#		
#
#   2.2 Classes do Projeto
#       Foram modeladas classes que ao mesmo tempo podesse ser autoexplicativas e mais próximas possíveis das
#	entidades envolvidas de um protocolo.
#		Fica assim o trabalho resumido o pacotes
#			✓ Default (Principal e *.jar)
#			✓ services/
#			✓ servidor/
#			✓ client/
#       Vejamos um pouco sobre cada um desses pacotes abaixo e sua ação no projeto:
#           ->Default
#              Contem a classe principal que está exclusivamente ligada ao processo de execução do protocolo, ela exerce a
#				mesma função de levantar os servidores;
#	    -> services/
#		Pasta onde foram implementado classes que tem por finalidade prover serviços para o cliente e o servidor
#		
#		-Arquivo.java : Classe responsável por manipular os arquivos utilizados no programa;
#
#		-ArquivoInfo.java : Classe que faz a persistencia das informações dos arquivos de cada peer;
#
#		-Comando.java: Classe que tem todos os comandos possiveis no protocolo PCMJ e métodos de conversão auxiliares;
#
#           	-Configuração.java: Esta é a Classe contém todas as configurações basicas para o funcionamento do programa,
#		 entre eles estão o ip do servidor, chave e porta padrão.Esta Classe é um SingleTon;
#	    
#		-Formater.java: Classe responsável por formatar todos os comandos do protocolo para o formato JSON
#			utilizado na troca de mensagens entre os peers e também com o servidor intermediário;
#	
#	    -> servidor/
#		
#		-IntermediaryServer.jar 
#
#	    -> client/
#
#		-HttpClient.java: Classe responsável por executar o client Http
#
#		-HttpServer.java: Contém a classe que que será executada por THREAD e roda o servidor HTTP
#
#		-PcmjClient.java: Contém a parte de cominucação com o protocolo PCMJ parte cliente
#		
#		-PcmjServidor.java: Contém a parte de cominucação com o protocolo PCMJ parte servidor
#
#		-Peer.java: Classe contém um peer propriamente dito é manipulada dentro da classe seeds após uma instancia.
#
#		-Seeds.java: Classe principal de manipulação do objeto, recebe e envia comandos referentes ao protocolo
#	   
#	     Na Versão 2.0 do projeto Bragi foi inserido SSL e autenticação por certificação.
#		Onde a autoridade certificadora foi criada apartir do OpenSSL, assim como os certificados.
#		Para verificar se a comunicação entre os peers estava realmente segura foi utilizado
# 		o software wireshark para monitoramento de rede.
#        
#
#####################################################################################################################
#   3. Execução
#####################################################################################################################
#		
#	O programa Bragi conta com algumas específicações que devem ser atendidas para poder efetuar a troca 
#	de arquivos com os outros peers.
#	
#	1. Classe Configuracao.java:
#		Antes de começar a execução deve-se mudar dois atributos desta classe 
#			interfaceRede = "<suaIterface>";
#			ipServidor = "<IPdoServidor>";
#		onde <suaINterface> deve ser sua interface de rede que está sendo utilizada (Ex: eth0, wlan, ...) e
#	 <IPdoServidor> é o IP do seu servidor intermediário;
#      
#   	2. Configuração Mínima
#       	✓ Java versão 1.6 ou mais.
#       	✓ Execução do Bragi é feita através do Makefile.          
#
#####################################################################################################################
# 4. Resultados e Conclusão
#####################################################################################################################
#
#       O resultado da execução é a comunicação através do protocolo PCSMJ com outros peers selecionados e a realização 
# 	de uma listagem de arquivos disponiveis para transferencia de forma segura pelo peer conectador além da transmissão 
#    do arquivo selecionado. 
#	Salientamos que na implementação foi utilizado Threads para a execução dos servidores PCSMJ e HTTP, sendo que 
#	o protocolo PCSMJ foi desenvolvido por nós e os demais listados abaixo:
#
#	-André Guimarães Peil
#	-Aline Rodrigues Tonini
#	-Daniel Krolow Retzlaff
#	-Giovani Torres
#	-Guilherme Cousin
#	-Glauco Roberto Munsberg do Santos
#	-Valesca do Amaral Nunes
#
#####################################################################################################################
# 5.Bibliografia
#####################################################################################################################
#
#   KUROSE, J. F.; ROSS, K. W. Redes de Computadores e a Internet: uma nova abordagem. Tradução de Arlete Simille Marques.
#   São Paulo: Addison Wesley, 2003. cap. 6, p. 377-440.
#	
#	OpenSSL : disponível em https://www.openssl.org/
#
#   Wireshark: disponível em http://www.wireshark.org/
#####################################################################################################################
