import java.util.Scanner;

import services.*;
import client.HttpServer;
import client.PcsmjCliente;
import client.PcsmjServidor;
import client.Peer;
import client.Seeds;

public class Principal {
	private PcsmjCliente clientPcmj;
	private PcsmjServidor servidorPcmj;
	private HttpServer servidorHttp;
	private Configuracao config;
	private Formater formatador;
	private Scanner leitor;
	private Seeds seeds;
	/**
	 * @param args
	 */
	public Principal() {
		
		
		config = Configuracao.getConfig();
		formatador = Formater.getFormater();
		seeds = Seeds.getSeeds();
		leitor = new Scanner(System.in);
		
		System.setProperty("javax.net.ssl.keyStore",config.getP12Address());
        System.setProperty("javax.net.ssl.keyStorePassword",config.getP12Password());
        System.setProperty("javax.net.ssl.keyStoreType", "PKCS12");
        System.setProperty("javax.net.ssl.trustStore",config.getTrustStoreAddress());
        System.setProperty("javax.net.ssl.trustStorePassword",config.getTrustStorePassword());
		
		servidorPcmj = new PcsmjServidor();
		clientPcmj = new PcsmjCliente();
		servidorHttp = new HttpServer();

	}

	public void comecar() {
		
		

		servidorPcmj.start();
		servidorHttp.start();
		Integer escolha = 1;
		
		while(escolha > 0 && escolha < 10){
			
			System.out.println("*******************************");
			System.out.println(" 1) Conectar ao Servidor");
			if(seeds.temPeer()){
				System.out.println(" 2) Conecta a um Peer");
			}
			System.out.println(" 3) Sair");
			escolha = leitor.nextInt();
			switch(escolha){
				case 1:
					clientPcmj = new PcsmjCliente();
					clientPcmj.enviarMensagemViaSocketServer(config.getServidorIp(), formatador.getPing(config.getServidorIp()).toString());
					break;
				case 2:
					if(seeds.temPeer()){
						Integer peer = seeds.getEscolhePeer();
						if(peer != seeds.getSize()){
							Peer peerTemp = seeds.getPeerPorId(peer);
							clientPcmj = new PcsmjCliente();
							clientPcmj.enviarMensagemViaSocketPeer(peerTemp.getIp(), formatador.getPing(peerTemp.getIp()).toString());
						}
					}
					break;
				case 3:
					System.out.println("Finalizado!");
					System.exit(0);
					break;
				default:
					break;
			}
			
		}
		
	}

	public static void main(String[] args) {

		Principal principal = new Principal();
		principal.comecar();
		
	}

}