package client;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.UnknownHostException;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.logging.Logger;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import services.Arquivo;
import services.ArquivoInfo;
import services.Comando;
import services.Configuracao;
import services.Formater;

/**
 *  Esta é a Classe que contém tudos os peers e com isso 
 *  	todos os métodos necessários para fazer a interação 
 *  	deles entre eles e com o servidor.
 *  Esta Classe é um SingleTon.
 */
public class Seeds{
	
	private static volatile Seeds sedders = null;
	private LinkedList<Peer> peers;
	private Arquivo arquivos;
	private Formater formatador;
	private Configuracao config;
	private Peer servidor;
	private Peer tempPeer;
	private JSONObject msgJson;
	private Logger log;
	private Scanner leitor;
	private HttpCliente http;
	
	private Seeds(){
		this.arquivos = Arquivo.getArquivo();
		this.formatador = Formater.getFormater();
		this.config = Configuracao.getConfig();
		this.log = config.getLog();
		this.servidor = new Peer(config.getServidorIp(),Comando.none,false);
		this.peers = new LinkedList<Peer>();
		this.leitor = new Scanner(System.in);
	}
	
	public static Seeds getSeeds(){
		if (sedders == null) {
        	synchronized (Seeds.class){
        		if (sedders == null) {
        			sedders = new Seeds();
        		}
        	}
        }
		
        return sedders;
	}
	
	public boolean temPeer(){
		if (peers.size() != 0){
			return true;
		} else {
			return false;
		}
		
	}
	
	public void limpaLista(){
		peers.clear();
	}
	
	public Integer getSize(){
		return peers.size();
	}
	
	public Peer getPeerPorId(Integer id){
		return peers.get(id);
	}
	
	/**
	 * Este metodo recebe a mensagem e decodifica ela para o padrao de manuseio
	 */
	public boolean recebeMensagem(String ip, String string) throws JSONException{
		if(string.equals(null)){
			return false;
		}
		try{
			msgJson = this.formatador.getJSON(string);
		} catch  (JSONException e) {
			log.info("Erro: " + string);
			return false;
		}
		return true;
	}
	
	public String preparaMensagemDeRetorno(){
		if(msgJson.getString("sender").equals(servidor.getIp())){
			return this.preparaMensagemParaServidor();
		}else{
			
			return this.preparaMensagemParaPeer();
		}
	}
	
	public String preparaMensagemParaServidor(){
		servidor.setJSON(msgJson);
		
		switch(servidor.getComandoAtual()){
			case ping:
				servidor.setComandoAtual(Comando.pong);
				servidor.setStatus(0);
				log.info("Ping: " + servidor.getJson());
				return this.formatador.getPong(servidor.getIp(), 100).toString();	
			case pong:
				servidor.setComandoAtual(Comando.authenticate);
				servidor.setStatus(0);
				log.info("Pong: "+servidor.getJson());
				return this.formatador.getAuthenticate(servidor.getIp(), config.getNickname(), config.getPublicKey()).toString();
			case authenticate_back:
				servidor.setComandoAtual(Comando.agent_list);
				servidor.setStatus(0);
				log.info("Autentica-back: "+servidor.getJson());
				return this.formatador.getAgentList(servidor.getIp()).toString();
			case agent_list_back:			
				servidor.setStatus(0);
				if(servidor.getStatus() == 400)
					return Comando.getStatusMensagem(400);
				if (servidor.getStatus() == 401)
					return Comando.getStatusMensagem(401);	
				
				JSONArray array = new JSONArray();
				array = msgJson.getJSONArray("back");
				limpaLista();
				for (int i = 0; i < array.length(); i++){
					Peer a = new Peer();
					a.setIp(array.get(i).toString());
					peers.add(a);
					System.out.println(peers.get(i).getIp());
				}
				log.info("agent-list-back:"+servidor.getJson());
				return null;
			default:
				servidor.setComandoAtual(Comando.none);
				log.severe("Commando Não encontrada:Servidor("+servidor.getIp()+")"+" JSON:"+servidor.getJson());
				return null;
		}
	}
	
	public String preparaMensagemParaPeer(){
		tempPeer = null;
		tempPeer = getPeerFromIp(msgJson.getString("sender"));
		if(tempPeer == null){
			this.setNovoPeer(msgJson.getString("sender"), Comando.getComandoBackFromString(msgJson.getString("command")), false);
			tempPeer = this.getPeerFromIp(msgJson.getString("sender"));
		}
		tempPeer.setJSON(msgJson);
		
		switch(tempPeer.getComandoAtual()){
			case ping:
				log.info("Ping:"+tempPeer.getIp());
				tempPeer.setComandoAtual(Comando.pong);
				tempPeer.setStatus(100);
				return this.formatador.getPong(tempPeer.getIp(), 100).toString();
			case pong:
				log.info("Pong:"+tempPeer.getIp());
				tempPeer.setComandoAtual(Comando.authenticate);
				tempPeer.setStatus(0);
				return this.formatador.getAuthenticate( tempPeer.getIp(), config.getNickname(), config.getPublicKey()).toString();
			case authenticate:
				log.info("Autenticate:"+tempPeer.getJson());
				tempPeer.setComandoAtual(Comando.authenticate_back);
				if(tempPeer.getProtocol().equals("pcsmj") && tempPeer.getNickname() != "" && tempPeer.getSender() != "" && tempPeer.getReceptor() != "" && tempPeer.getNickname() != ""){
					
					/**
					 * Antes de retornar o authentificate-back deve-se
					 * 	conectar ao servidor e verificar se a chave pública dele
					 * 	está correta
					 */
					
					SSLSocketFactory sslsocketfactory = (SSLSocketFactory) SSLSocketFactory.getDefault();
		            SSLSocket sslsocket = null;
		            DataOutputStream outToServer = null;
					String enviar = null;
					BufferedReader inFromServer = null;
					String retorno = null;
		            try {
						sslsocket = (SSLSocket) sslsocketfactory.createSocket(config.getServidorIp(), config.getPortaPcsmj());
						outToServer = new DataOutputStream(sslsocket.getOutputStream());
						enviar = this.formatador.getCertify(tempPeer.getNickname(), tempPeer.getPublicKey(), config.getServidorIp()) + "\n";
						inFromServer = new BufferedReader(new InputStreamReader(sslsocket.getInputStream()));
						outToServer.writeBytes(enviar);
						log.severe("Certify sended: "+enviar);
						retorno = inFromServer.readLine();
						sslsocket.close();
		            } catch (UnknownHostException e) {
		            	log.severe("Error - Host desconhecido: "+e.getMessage());
						e.printStackTrace();
					} catch (IOException e) {
						log.severe("Error - falha de I/O: "+e.getMessage());
					}
		             
		            JSONObject retornoJSON = this.formatador.getJSON(retorno);
		            log.severe("Certify Back :"+retornoJSON.toString());
		            if(retornoJSON.getString("status") == "302"){
		            	tempPeer.setAutenticado(true);
		            	return this.formatador.getAuthenticateBack(200, tempPeer.getIp()).toString();
		            }else{
		            	return this.formatador.getAuthenticateBack(Integer.parseInt(retornoJSON.getString("status")), tempPeer.getIp()).toString();
		            }
					
				}else{
					tempPeer.setAutenticado(false);
					tempPeer.setStatus(400);
					log.warning("Authenticate:"+tempPeer.getIp()+" fora do padrão");
					return this.formatador.getAuthenticateBack(400, tempPeer.getIp()).toString();
				}
			case authenticate_back:
				log.info("Authenticate-back:"+tempPeer.getIp());
				if(tempPeer.getStatus() == 200){
					tempPeer.setAutenticado(true);
					tempPeer.setComandoAtual(Comando.archive_list);
					log.info("Authenticate-back:"+tempPeer.getIp()+ tempPeer.estaAutenticado());
					return this.formatador.getArchiveList(tempPeer.getIp()).toString();
				}else if(tempPeer.getStatus() == 203){
					log.warning("Authenticate-back:"+tempPeer.getIp()+" Status:203"+Comando.getStatusMensagem(203));
					return null;
				}else if(tempPeer.getStatus() == 400){
					log.warning("Autenticate-back:"+tempPeer.getIp()+" Status:400"+Comando.getStatusMensagem(400));
					return null;
				}else if(tempPeer.getStatus() == 501){
					log.info("Autenticate-back:"+tempPeer.getIp()+" Status:501"+Comando.getStatusMensagem(501));
					return null;
				}else{
					log.severe("Autenticate-back:"+tempPeer.getIp()+" Status:"+tempPeer.getStatus()+" "+Comando.getStatusMensagem(tempPeer.getStatus()));
					return null;
				}
			case archive_list:
				log.info("Archive-list:"+tempPeer.getIp());
				tempPeer.setComandoAtual(Comando.archive_list_back);
				if(tempPeer.estaAutenticado()){
					if(tempPeer.getProtocol().equals("pcmj") && tempPeer.getSender() != "" && tempPeer.getReceptor() != ""){
						tempPeer.setStatus(200);
						return this.formatador.getArchiveListBack(arquivos.listaDeArquivos(), 200, tempPeer.getIp()).toString();
					}else{
						tempPeer.setStatus(400);
						return this.formatador.getArchiveListBack(null, 400, tempPeer.getIp()).toString();
					}
				}else{
					log.severe("Archive-list from "+tempPeer.getIp()+" Não autenticado");
					tempPeer.setStatus(401);
					return this.formatador.getArchiveListBack(null, 401, tempPeer.getIp()).toString();
				}
			case archive_list_back:
				log.info("Archive-list-back:"+tempPeer.getIp()+ tempPeer.estaAutenticado());
				if(tempPeer.estaAutenticado()){
					log.severe("Autenticado");
					if(tempPeer.getStatus() == 200){
                                                tempPeer.limpaListaArquivos();
						tempPeer.setListaDeArquivos(tempPeer.getBack());
						int arquivoId = getUmArquivoDoPeer(tempPeer);
						if(arquivoId < tempPeer.getListaDeArquivos().size()){
							return this.formatador.getArchiveRequest(arquivoId, tempPeer.getIp()).toString();
						}else{
							return null;
						}
					}else if(tempPeer.getStatus() == 400){
						log.severe("Archive-list-back:"+tempPeer.getIp()+" Status:"+tempPeer.getStatus()+" "+Comando.getStatusMensagem(tempPeer.getStatus()));
					}else if(tempPeer.getStatus() == 401){
						log.severe("Archive-list-back:"+tempPeer.getIp()+" Status:"+tempPeer.getStatus()+" "+Comando.getStatusMensagem(tempPeer.getStatus()));
					}else if(tempPeer.getStatus() == 501){
						log.info("Archive-list-back:"+tempPeer.getIp()+" Status:"+tempPeer.getStatus()+" "+Comando.getStatusMensagem(tempPeer.getStatus()));
					}else{
						log.severe("Autenticate-list-back:"+tempPeer.getIp()+" Status:"+tempPeer.getStatus()+" "+Comando.getStatusMensagem(tempPeer.getStatus()));
						return null;
					}
				}else{
					log.severe("Archive-list-back:"+tempPeer.getIp()+" não solicitado");
					return null;
				}
			case archive_request:
				log.info("Archive-request:"+tempPeer.getIp());
				if(tempPeer.estaAutenticado()){
					tempPeer.setComandoAtual(Comando.archive_list_back);
					if(tempPeer.getProtocol().equals("pcmj") && tempPeer.getSender() != "" && tempPeer.getReceptor() != "" && tempPeer.getId() >= 0 && arquivos.existeArquivo(tempPeer.getId())){
						tempPeer.setStatus(302);
						return this.formatador.getArchiveRequestBack(302, tempPeer.getId(), arquivos.getTamanhoPeloId(tempPeer.getId()), arquivos.getMD5PeloId(tempPeer.getId()), arquivos.getHttpAddressPorId(tempPeer.getId()), tempPeer.getIp()).toString() ;
					}else{
						tempPeer.setStatus(400);
						return this.formatador.getArchiveRequestBack(400, tempPeer.getId(), 0, null, null, tempPeer.getIp()).toString();
					}
				}else{
					tempPeer.setStatus(401);
					log.severe("Archive-request:"+tempPeer.getIp()+" Não autenticado");
					return this.formatador.getArchiveRequestBack(401, tempPeer.getId(), null, null, null, tempPeer.getIp()).toString();
				}
			case archive_request_back:
				log.info("Archive-request-back:"+tempPeer.getJson());
				if(tempPeer.estaAutenticado()){
					if(tempPeer.getStatus() == 302){
						try{
							this.http = new HttpCliente(tempPeer.getIp());
							
						}catch(Exception e){
							e.printStackTrace();
						}
						 
						this.http.getSolicitacao(tempPeer.getListaDeArquivos().get(tempPeer.getId()), tempPeer.getIp(),"/"+tempPeer.getHttpAddress());
						 return null;
					}else if(tempPeer.getStatus() == 400){
						log.severe("Archive-request-back:"+tempPeer.getIp()+" Status:"+tempPeer.getStatus()+" "+Comando.getStatusMensagem(tempPeer.getStatus()));
						return null;
					}else if(tempPeer.getStatus() == 404){
						log.severe("Archive-request-back:"+tempPeer.getIp()+" Status:"+tempPeer.getStatus()+" "+Comando.getStatusMensagem(tempPeer.getStatus()));
						return null;
					}else if(tempPeer.getStatus() == 401){
						log.severe("Archive-resquest-back:"+tempPeer.getIp()+" Status:"+tempPeer.getStatus()+" "+Comando.getStatusMensagem(tempPeer.getStatus()));
						return null;
					}else if(tempPeer.getStatus() == 501){
						log.info("Archive-request-back:"+tempPeer.getIp()+" Status:"+tempPeer.getStatus()+" "+Comando.getStatusMensagem(tempPeer.getStatus()));
						return null;
					}else{
						log.severe("Autenticate-request-back:"+tempPeer.getIp()+" Status:"+tempPeer.getStatus()+" "+Comando.getStatusMensagem(tempPeer.getStatus()));
						return null;
					}
				}else{
					log.severe("Archive-list-back:"+tempPeer.getIp()+" não solicitado");
					return null;
				}
			case end_connection:
				log.info("End-connection:"+tempPeer.getJson());
				this.removePeer(tempPeer);
				return null;
			default:
				log.info("Not-recognized:"+msgJson.toString());
				tempPeer.setComandoAtual(Comando.none);
				log.severe("Comando não encontado:Peer("+tempPeer.getIp()+")"+" JSON:"+tempPeer.getJson().toString());
				return null;
		}
	}
	public String enviaMensagem(){
		
		String stringJson = preparaMensagemDeRetorno();
		return stringJson;
	}
	
	
	/**
	 * Caso leventa uma Exception então deve-ser seguir um processo
	 * 	de forma que se responda ao rementende.ß
	 * @param Integer ip - IP de quem enviou a mensagem
	 * @param String mensagem - Mensagem que o JSON não consegui tratar 
	 */
	public void tratarMensagem(String ip, String mensagem){
		if(this.isServidor(ip)){
			Comando tempComando = Comando.getComandoFromString(mensagem);
			log.severe("Erro - TratarMensage: "+tempComando);
		}else{
			tempPeer = this.getPeerFromIp(ip);
			if(tempPeer == null){
				this.setNovoPeer(ip, Comando.none, false);
			}else{
				
			}
		}
		
	}
	
	public void setNovoPeer(String ip, Comando comando, boolean autenticado){
		peers.add(new Peer(ip,comando, autenticado));
	}
	
	public void removePeer(Peer peerToRemove){
		peers.remove(peerToRemove);
	}
	
	public Peer getPeerFromIp(String ip){
		
		if (peers.isEmpty())
			return null;
		
		for(Peer temp : peers){
			if(ip.equals(temp.getIp())){
				return temp;
			}
		}
		return null;
	}

	public boolean isServidor(String ip){
		
		if(servidor.getIp().equals(ip)){
			return true;
		}else{
			return false;
		}
	}
	
	
	public Integer getUmArquivoDoPeer(Peer peer){
		System.out.println("-------------------------------------------");
		System.out.println("Selecione um arquivo do Peer "+peer.getIp());
		System.out.println("");
		//System.out.println(peer.getListaDeArquivos().getFirst());
		Integer numero = -1;
		
		for(ArquivoInfo file : peer.getListaDeArquivos()){
			numero++;
			System.out.println(" "+numero+") "+file.getNome() +" "+file.getSize()+"KB");
		}
		System.out.println(" "+(numero+1)+") SAIR");
		System.out.println("-------------------------------------------");
		do{
			numero = leitor.nextInt();
		} while(numero < 0 && numero >= peer.getListaDeArquivos().size());
		return numero;
	}
	
	public Integer getEscolhePeer(){
		System.out.println("-------------------------------------------");
		System.out.println("Selecione um Peer conectar ");
		System.out.println("");
		Integer numero = 0;
		for(Peer temp : peers){
			numero++;
			System.out.println(" "+numero+") "+temp.getIp());
		}
		System.out.println(" "+(numero+1)+") SAIR");
		System.out.println("-------------------------------------------");
		do{
			numero = leitor.nextInt();
		} while (numero < 0 && numero >= peers.size());
		return numero-1;
	}

	
}