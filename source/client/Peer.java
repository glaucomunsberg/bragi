package client;

import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import services.*;
import org.json.*;
/**
 *  Esta é a Classe representa um peer que está conectado
 *  	ao programa. Nele há informações como IP, Data
 *  	comando, status e etc
 */
public class Peer{
	private String ip;
	private Comando comandoAtual;
	private Boolean estaAutenticado;
	private Date dataAtua;
	public Integer status;
	private Socket esteSocket;
	private JSONObject atual;
	private LinkedList<ArquivoInfo> arquivos;
	Formater formatador = null;
	
	public Peer(){ 
		
		dataAtua = new Date();
		comandoAtual = Comando.none;
		this.estaAutenticado = false;
		this.arquivos = new LinkedList<ArquivoInfo>();
		this.status = 0;
	}
	
	public Peer(String ip, Comando comando, boolean autenticado){
		
		this.setIp(ip);
		this.setComandoAtual(comando);
		this.setAutenticado(autenticado);
		this.status = 0;
	}
	
	public Peer(Peer copia){
		
		this.ip = copia.ip;
		this.comandoAtual = copia.comandoAtual;
		this.dataAtua = copia.dataAtua;
		this.estaAutenticado = copia.estaAutenticado;
		this.status = copia.status;
		this.arquivos = copia.arquivos;
	}
	
	public void limpaListaArquivos(){
		arquivos.clear();
	}
	
	public void setIp(String ip){
		this.ip = ip;
	}
	
	public boolean isIp(String ip){
		
		if(ip.equals(getIp())){
			return true;
		}else{
			return false;
		}
	}
	
	public String getIp(){
		
		return this.ip;
	}
	
	public void setComandoAtual(Comando comandoAtualRecebido){
		comandoAtual = comandoAtualRecebido;
		dataAtua = new Date();
	}
	
	public Comando getComandoAtual(){
		
		return comandoAtual;
	}
	
	public Integer getStatus(){
		
		return status;
	}
	
	public void setStatus(Integer statusRecebido){
		status = statusRecebido;
	}
	public void setAutenticado(boolean autenticado){
		estaAutenticado = autenticado;
	}
	
	public boolean estaAutenticado(){
		
		return this.estaAutenticado;
	}
	
	public String getDataUltimaModificacao(){
		
		SimpleDateFormat formatarDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss"); 
		return formatarDate.toString();
	}
	
	public void setSocket(Socket socket){
		esteSocket = socket;
	}
	
	public Socket getSocket(){
		
		return this.esteSocket;
	}
	
	public void setJSON(JSONObject atual){
		
		this.atual = atual;
		try{
			if (atual.toString().contains("status"))
				this.setStatus(Integer.parseInt((atual.getString("status"))));	
			
		} catch (JSONException e){
			e.printStackTrace();
		}
			
		this.setComandoAtual(Comando.getComandoFromString(atual.getString("command").toString()));
	}
	
	public String getProtocol(){
		
		return this.atual.getString("protocol");
	}
	
	public String getCommand(){
		//System.out.println("getCommand:"+atual.getString("command"));
		return this.atual.getString("command");
	}
	
	public String getSender(){
		
		return this.atual.getString("sender");
	}
	
	public String getReceptor(){
		
		return this.atual.getString("receptor");
	}

	/**
	 * Retorna uma array JSON com os arquivos
	 * Exemplo dee retorno : ["xuxa","fsdofjdf","joazinho"]
	 * 
	 */
	public JSONArray getBack() throws JSONException{
		System.out.println("BACK: "+this.atual.getJSONArray("back").toString());
		JSONArray aux = this.atual.getJSONArray("back");
		return aux;
	}
	
	/**
	 * Retorna um JSONObject dos arquivos
	 * Exemplo de Retorno : {"3":"joazinho","2":"fsdofjdf","1":"xuxa"}
	 * Foi inserido um id pra talvez a pessoa escolher
	 */
	public JSONObject getBackComID() throws JSONException{
		JSONArray aux = this.atual.getJSONArray("back");
		JSONObject back = new JSONObject();
		for(int i = 0; i < aux.length(); i++){
			back.put( String.valueOf(i+1),aux.getString(i));
		}
		
		return back;
	}
	
	public String getPublicKey(){
		return this.atual.getString("public_key");
	}
	
	public String getNickname(){
		return this.atual.getString("nick_name");
	}
	
	public Integer getId(){
		return this.atual.getInt("id");
	}
	
	public String getHttpAddress(){
		return this.atual.getString("http_address");
	}
	
	public Integer getSize(){
		return this.atual.getInt("size");
	}
	
	public String getMd5(){
		return this.atual.getString("md5");
	}
	
	public String getJson(){
		//System.out.println("getJson:"+this.atual.toString());
		return this.atual.toString();
	}
	
	public void setListaDeArquivos(JSONArray aux) {
		for (int i = 0; i < aux.length(); i++) {
			arquivos.add(new ArquivoInfo(aux.getJSONObject(i).getString("id").toString(),aux.getJSONObject(i).getString("name").toString(),aux.getJSONObject(i).getString("size").toString()));
		}
	}
	public JSONArray getListaDeArquivosJSON(){
		return new JSONArray(arquivos);
	}
	
	public LinkedList<ArquivoInfo> getListaDeArquivos(){
		return this.arquivos;
	}
}