package client;

import java.io.*;
import java.net.*;
import java.util.logging.Logger;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import org.json.JSONException;

import services.Configuracao;

/**
 *  A classe Cliente aqui representa a parte que envia mensagens
 *  	aos demais peers e ao servidor
 */
public class PcsmjCliente{
	private Configuracao config;
	private Logger log;
	private Seeds sedds;
	
	public PcsmjCliente() {
		config = Configuracao.getConfig();
		sedds = Seeds.getSeeds();
	}

	public void enviarMensagemViaSocketServer(String ip, String mensagemJSON) {		
		
			try {
				SSLSocketFactory sslsocketfactory = (SSLSocketFactory) SSLSocketFactory.getDefault();
	            SSLSocket sslsocket = (SSLSocket) sslsocketfactory.createSocket(ip, config.getPortaPcsmj());
				DataOutputStream outToServer = new DataOutputStream(sslsocket.getOutputStream());
				BufferedReader inFromServer = new BufferedReader(new InputStreamReader(sslsocket.getInputStream()));
				
				outToServer.writeBytes(mensagemJSON + "\n");
				
				String retorno = inFromServer.readLine();
				
				if(sedds.recebeMensagem(ip, retorno)){
					String temp = sedds.preparaMensagemDeRetorno();
					if(temp != null){
						this.enviarMensagemViaSocketServer(ip, temp);
					}
				}
					
				sslsocket.close();
			} catch (IOException e) {
				//log.severe("Error - ServerSocket não pode ser instanciado: "+e.getMessage());
				e.printStackTrace();
			} catch( JSONException e){
				//log.severe("Error - JSONExpetion ocorreu: "+e.getMessage());
				e.printStackTrace();
			} catch(SecurityException e){
				//log.severe("Error - Problema de Segurança: "+e.getMessage());
	        	e.printStackTrace();
	        }catch(Exception e){
				//log.severe("Error - Exception Geral: "+e.getMessage());
				e.printStackTrace();
			}
	}
	
	public void enviarMensagemViaSocketPeer(String ip, String mensagemJSON) {		
		
		try {
			SSLSocketFactory sslsocketfactory = (SSLSocketFactory) SSLSocketFactory.getDefault();
            SSLSocket sslsocket = (SSLSocket) sslsocketfactory.createSocket(ip, config.getPortaPcsmj());
			DataOutputStream outToServer = new DataOutputStream(sslsocket.getOutputStream());
			BufferedReader inFromServer = new BufferedReader(new InputStreamReader(sslsocket.getInputStream()));
			
			outToServer.writeBytes(mensagemJSON + "\n");
			
			String retorno = inFromServer.readLine();
			
			if(sedds.recebeMensagem(ip, retorno)){
				String temp = sedds.preparaMensagemDeRetorno();
				if(temp != null){
					this.enviarMensagemViaSocketServer(ip, temp);
				}
			}
				
			sslsocket.close();
		} catch (IOException e) {
			//log.severe("Error - ServerSocket não pode ser instanciado: "+e.getMessage());
			e.printStackTrace();
		} catch( JSONException e){
			//log.severe("Error - JSONExpetion ocorreu: "+e.getMessage());
			e.printStackTrace();
		} catch(SecurityException e){
			//log.severe("Error - Problema de Segurança: "+e.getMessage());
        	e.printStackTrace();
        }catch(Exception e){
			//log.severe("Error - Exception Geral: "+e.getMessage());
			e.printStackTrace();
		}
}
	

}