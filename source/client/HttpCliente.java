package client;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Logger;

import services.ArquivoInfo;
import services.Configuracao;

public class HttpCliente {

	private Configuracao config;
	private String HTTPVersion;
	private String host;
	private int port;
	private Logger log;
	

	/**
	 * Construtor do cliente HTTP
	 * @param host host para o cliente acessar
	 * @param port porta de acesso
	 */
	public HttpCliente(String host) {
		this.HTTPVersion = "HTTP/1.1";
		this.host = host;
		config = Configuracao.getConfig();
		this.port = config.getPortaHttps();
		log = config.getLog();
	}

	/**
	 * Realiza uma requisição HTTP e devolve uma resposta
	 * @param path caminho a ser feita a requisição
	 * @return resposta do protocolo HTTP
	 * @throws UnknownHostException quando não encontra o host
	 * @throws IOException quando há algum erro de comunicação
	 */
	public String getURIRawContent(ArquivoInfo arquivo, String path) throws UnknownHostException,IOException {
		Socket socket = null;
		try {
			// Abre a conexão
			socket = new Socket(this.host, this.port);
			PrintWriter saida = new PrintWriter(socket.getOutputStream(), true);
			BufferedReader entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));

			// Envia a requisição
			saida.println("GET " + path + " " + HTTPVersion);
			saida.println("Host: " + this.host);
			saida.println("Connection: Close");
			saida.println();

			boolean continuar = true;
			@SuppressWarnings("unused")
			StringBuffer stringBuff = new StringBuffer();
			
			//FileOutputStream fos = new FileOutputStream("pathname");
			FileOutputStream arq; 
			
			try {
			 	arq = new FileOutputStream(System.getProperty("user.dir")+"/source/pcmjFiles/"+arquivo.getNome());
			
			} catch (IOException e){
				e.printStackTrace();
				arq = new FileOutputStream(System.getProperty("user.dir")+"/source/pcmjFiles/"+arquivo.getNome()+"1");
				
			}
			
			
			// recupera a resposta quando ela estiver disponível
			while (continuar) {
				if (entrada.ready()) {
					int i = 0;
					while ((i = entrada.read()) != -1) {
						arq.write(i);
					}
					continuar = false;
				}
			}
			arq.close();
			return "true";
		}
		finally {
			if (socket != null) {
				socket.close();
			}
		}
	}
	
	public boolean getSolicitacao(ArquivoInfo arquivo, String ip,String URL){
		HttpCliente client = new HttpCliente(ip);
		try {
			client.getURIRawContent(arquivo, URL);
			return true;
		} catch (UnknownHostException e) {
			log.severe("Host Desconhecido "+e);
		} catch (IOException e) {
			return false;
		}
		return true;
	}

}
