package client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Logger;

import org.http.utils.request.Request;
import org.http.utils.response.Response;
import org.http.utils.response.ResponseFactory;

import services.Configuracao;

public class HttpServer extends Thread {


	private String host;
	private int portaHttp;
	private Configuracao config;
	private Logger log;

	/**
	 * Construtor do servidor de HTTP
	 * 
	 * @param host
	 *            host do servidor
	 * @param portaHttp
	 *            porta do servidor
	 */
	public HttpServer() {
		super();
		config = Configuracao.getConfig();
		log = config.getLog();
		this.host = config.getMeuIP(); //localhost
		this.portaHttp = config.getPortaHttps();
	}

	/**
	 * Inicia o servidor e fica escutando no endereço e porta especificada no
	 * construtor
	 */
	public void run() {
		ServerSocket serverSocket = null;
		
		log.info("Iniciando Servidor HTTP");
		
		try {
			serverSocket = new ServerSocket(portaHttp, 1,InetAddress.getByName(host));
		} catch (IOException e) {
			log.severe("Erro ao iniciar servidor HTTP ServerSocket "+e);
			return;
		}
		log.info("Log - Aberta Servidor HTTP no endereço: " +this.host+ ":" + this.portaHttp);

		// Fica esperando pela conexão cliente
		while (true) {
			log.fine("Aguardando Conexão...");
			Socket socket = null;
			InputStream input = null;
			OutputStream output = null;
			try {
				socket = serverSocket.accept();
				input = socket.getInputStream();
				output = socket.getOutputStream();

				String requestString = converteStreamParaString(input);
				log.info("Log - Conexão recebida. Conteúdo:\n" + requestString);
				Request request = new Request();
				request.parse(requestString);

				// recupera a resposta de acordo com a requisicao
				
				log.severe("URL ABSOLUTA - "+System.getProperty("user.dir")+"/source/pcmjFiles"+request.getUri());
				File arquivo = new File(System.getProperty("user.dir")+"/source/pcmjFiles"+request.getUri());
				Response response = ResponseFactory.createResponse(request);
				String responseString = response.respond();
				log.info("Log - Resposta enviada. Conteúdo:\n" + responseString);
				 	byte[] bFile = new byte[(int) arquivo.length()];
				 
			        try {
			            //convert file into array of bytes
				    FileInputStream fileInputStream = new FileInputStream(arquivo);
				    fileInputStream.read(bFile);
				    fileInputStream.close();
			 
			        }catch(Exception e){
			        	e.printStackTrace();
			        }
				output.write(bFile);

				// Fecha a conexão
				socket.close();
				
			} catch (Exception e) {
				log.severe("Error - Ao executar servidor! "+e);
				continue;
			}
		}
	}

	private String converteStreamParaString(InputStream is) {

		if (is != null) {
			Writer writer = new StringWriter();

			char[] buffer = new char[2048];
			try {
				Reader reader = new BufferedReader(new InputStreamReader(is));
				int i = reader.read(buffer);
				writer.write(buffer, 0, i);
			} catch (IOException e) {
				log.severe("Error - Ao converter stream para string "+e);
				return "";
			}
			return writer.toString();
		} else {
			return "";
		}
	}

}
