package client;

import java.io.*;
import java.net.*;
import java.util.logging.Logger;

import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;

import org.json.JSONException;

import services.*;
/**
 * Servidor é a classe que está recebendo mensagens
 * 	tanto do servidor como dos demais peers
 */
public class PcsmjServidor extends Thread {
	private String clientSentence;
	@SuppressWarnings("unused")
	private SSLSocket welcomeSocket;
	private Configuracao config;
	private Logger log;
	private Socket connectionSocket;
	private BufferedReader inFromClient;
	private DataOutputStream outToClient;
	private Seeds seed;
	
	public PcsmjServidor() {
		connectionSocket = null;
		inFromClient = null;
		outToClient = null;
		config = Configuracao.getConfig();
		log = config.getLog();
		seed = Seeds.getSeeds();
	}

	@Override
	public void run() {
		System.setProperty("javax.net.ssl.keyStore",config.getP12Address());
        System.setProperty("javax.net.ssl.keyStorePassword",config.getP12Password());
        System.setProperty("javax.net.ssl.keyStoreType", "PKCS12");
        System.setProperty("javax.net.ssl.trustStore",config.getTrustStoreAddress());
        System.setProperty("javax.net.ssl.trustStorePassword",config.getTrustStorePassword());
        SSLServerSocketFactory sslserversocketfactory;
        SSLServerSocket sslserversocket = null;
		try {
			sslserversocketfactory = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
	        sslserversocket = (SSLServerSocket) sslserversocketfactory.createServerSocket(config.getPortaPcsmj());
	        
			log.info("Iniciando servidor PCMJ...");
		} catch (IOException e) {
			log.severe("Error - ServerSocket não pode ser instanciado "+e);
		}
		
		while (true) {
			try {
				welcomeSocket = (SSLSocket) sslserversocket.accept();
				inFromClient = new BufferedReader(new InputStreamReader(welcomeSocket.getInputStream()));
				outToClient = new DataOutputStream(welcomeSocket.getOutputStream());
				clientSentence = inFromClient.readLine();
				
				log.info("Sentença Recebida: "+clientSentence);
				try{
					seed.recebeMensagem(welcomeSocket.getInetAddress().getHostAddress(), clientSentence);
				}catch(JSONException e){
					seed.tratarMensagem(welcomeSocket.getInetAddress().getHostAddress(), clientSentence);
				}
			
				String retorno = seed.enviaMensagem();
				log.info(" depois"+ retorno);
				if(retorno != null){
					outToClient.writeBytes(retorno+ "\n");
				}
				welcomeSocket.close();
				
			} catch (IOException e) {
				log.severe("Error - Problema de I/O: "+e.getMessage());
				e.printStackTrace();
			}catch(SecurityException ex){
				log.severe("Error - Problema de Segurança: "+ex.getMessage());
	        	ex.printStackTrace();
	        }catch(Exception e){
				log.severe("Error - Exception Geral: "+e.getMessage());
				e.printStackTrace();
			}
			
		}
		
	}
	
}