package services;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
/**
 *  Esta é a Classe contém todas as configurações basicas
 *  	para o funcionamento do programa, entre eles estão
 *  	o ip do servidor, chave e porta padrão.
 *  Esta Classe é um SingleTon.
 */
public class Configuracao {

	private Integer portaPCSMJ;
	private Integer portaHTTPS;
	private String meuIp;
	private String nickname;
	private String interfaceRede;
	private String ipServidor;
	private String p12Address;
	private String p12Password;
	private String trustStoreAddress;
	private String trustStorePassword;
	private String pemAddress;
	private InetAddress IPAddress;
	private Logger logger;
	private static volatile Configuracao instance = null;

	private Configuracao(){
		
		portaPCSMJ = 6789;
		portaHTTPS = 5789;
		interfaceRede = "eth1";
		ipServidor = "192.168.1.101";
		
		nickname = "andreguipeil";
		p12Address = "/home/agpeil/git/bragi/source/libraries/andreguipeil-certificado.p12";
		pemAddress = "/home/agpeil/git/bragi/source/libraries/andreguipeil-certificado.pem";
		p12Password = "inferencia";
		trustStoreAddress = "/home/agpeil/git/bragi/source/libraries/myTrustStore.jks";
		trustStorePassword = "inferencia";
		
		logger = Logger.getLogger(Configuracao.class.getName()) ;
		FileHandler fh; 
		  
		try {
			fh = new FileHandler(System.getProperty("user.dir")+"Pcmj.log");
			logger.addHandler(fh); 
		} catch (SecurityException | IOException e1) {
			e1.printStackTrace();
		}   
		
		try {
			IPAddress = InetAddress.getByName("localhost");
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		
		try{
			Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
			while(interfaces.hasMoreElements()){
				NetworkInterface face = interfaces.nextElement();
				if(face.isLoopback() || !face.isUp())
					continue;
				Enumeration<InetAddress> endereco = face.getInetAddresses();
				while(endereco.hasMoreElements()){
					InetAddress addr = endereco.nextElement();
					if(face.getDisplayName().equals(this.interfaceRede) &&  addr.getHostAddress().indexOf(".") > -1){
						meuIp = addr.getHostAddress();
						
					}
					
				}
				logger.info("Endereço Local "+face.getDisplayName()+" "+meuIp);
			}
			
		}catch(SocketException e){
			logger.severe("Erro ao obter ip local "+e);
		}
		
	}
	
	public static Configuracao getConfig(){
		if (instance == null) {
        	synchronized (Configuracao.class){
        		if (instance == null) {
        			instance = new Configuracao();
        		}
        	}
        }
        return instance;
	}
	
	public String getPublicKey(){
		
		String command = "openssl x509 -pubkey -noout -in "+this.getPem();
		String line = null;  
		StringBuilder sb = new StringBuilder();
		try {
			// Execute command
			Process p = Runtime.getRuntime().exec(command);  
            BufferedReader in = new BufferedReader(  
                                new InputStreamReader(p.getInputStream()));  
           
            while ((line = in.readLine()) != null) {  
                 sb.append(line);
            }  
		} catch (IOException e) {
			//logger.error("Exception encountered", e);
		}
		return sb.toString().replace("-----BEGIN PUBLIC KEY-----", "").replace("-----END PUBLIC KEY-----", "");
	}
	
	public Integer getPortaPcsmj(){
		return portaPCSMJ;
	}
	
	public Integer getPortaHttps(){
		return portaHTTPS;
	}
	
	public String getTrustStoreAddress(){
		return this.trustStoreAddress;
	}
	
	public String getTrustStorePassword(){
		return this.trustStorePassword;
	}
	
	public String getP12Address(){
		return this.p12Address;
	}
	
	public String getP12Password(){
		return this.p12Password;
	}
	public String getMeuIPNome(){
		return this.IPAddress.getHostName();
	}
	
	public String getPem(){
		return this.pemAddress;
	}
	
	public String getMeuIPHost(){
		return this.IPAddress.getHostAddress();
	}
	
	public String getNickname(){
		return this.nickname;
	}
	public String getMeuIP(){
		
		return this.meuIp;
	}
	
	public String getServidorIp(){
		return this.ipServidor;
	}
	
	public void setServidor(String servidorIp){
		this.ipServidor = servidorIp;
	}
	
	public Logger getLog(){
		return this.logger;
	}

}
