package services;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.LinkedList;

import org.json.JSONObject;
/**
 *  Arquivo é a classe responsável pela manipulação
 *  	de arquivos pelo programa.
 */
public class Arquivo {

	public String diretorio;
	public LinkedList<String> listaArquivos;
	private static volatile Arquivo instance = null;
	private File arquivos;
	
	private Arquivo() {
		this.diretorio = System.getProperty("user.dir");// Colocar todo diretorio apartir do home
		this.listaArquivos = new LinkedList<String>();
		arquivos = new File(this.diretorio+"/source/pcmjFiles/");
	}
	
	public static Arquivo getArquivo(){
		if (instance == null) {
        	synchronized (Arquivo.class){
        		if (instance == null) {
        			instance = new Arquivo();
        		}
        	}
        }
        return instance;
	}
	
	public LinkedList<String> listaDeArquivos(){
		
		File afile[] = arquivos.listFiles();
		
		for (int i = 0; i < afile.length; i++) {
			File arquivos = afile[i];
			this.listaArquivos.add(arquivos.getName());
		}
		return this.listaArquivos;
	}
	
	public File getArquivoById(Integer id){
		File afile[] = arquivos.listFiles();
		return afile[id];
	}
	
	/**
	 *  pega só o nome do arquivo solicitado ** ver como fazer pra passar isso pra json.
	 * @param id
	 * @return
	 */
	public String getArquivo(int id){	
		return this.listaArquivos.get(id);
	}
	
	/**
	 * Retorna no formato do json informações que será usada no archive_requerest_back
	 */
	public JSONObject getInformacaoDoArquivoById(Integer id){
		
		return new JSONObject();
	}
	
	/*
	 * Retorna o md5 do arquivo
	 * */
	public String getMD5PeloId(Integer id){
		FileInputStream fis;
		String md5 = null;
		File afile[] = arquivos.listFiles();
		File arquivo = afile[id];
		try {
			fis = new FileInputStream(arquivo);
			md5 = org.apache.commons.codec.digest.DigestUtils.md5Hex(fis);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return md5;
	}
	
	/*Retona o tamanho em KB, pelo ID, se o arquivo representado pelo ID nao existe
	 * retorna tamanho ZERO;
	 * */
	public Integer getTamanhoPeloId(Integer id){
		File afile[] = arquivos.listFiles();
		if(this.existeArquivo(id)){
			File arquivo = afile[id];
			long size = arquivo.length();
			size = size/1024; //1024bytes são 1KB
			return (int)size;
			}else{
				return 0;
		}
	}
	
	/*
	 * Retorna se existe o arquivo
	 * */
	public boolean existeArquivo(Integer id){
		if(id >= 0 && id < arquivos.listFiles().length ){
			return true;
		}else{
			return false;
		}
	}
	
	public String getHttpAddressPorId(Integer id){
		return this.listaArquivos.get(id);
	}
}
class infoArquivos{
	String nomeArquivo;
	String tamanho;
	String md5;
}