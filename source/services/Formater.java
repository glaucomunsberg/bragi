package services;

import java.util.LinkedList;
import java.util.logging.Logger;

import org.json.*;

/**
 * 	A classe Formater formata os comandos do protocolo
 * 		para o formato JSON usado na troca de mensagens 
 *	Esta classe é um Singleton
 */
public class Formater{
	private static volatile Formater Formater = new Formater();
	private Configuracao config;
	private Logger log;
	private Formater(){
		
		config = Configuracao.getConfig();
		log = config.getLog();
	}
	
	public static Formater getFormater(){
		if (Formater == null) {
        	synchronized (Formater.class){
        		if (Formater == null) {
        			Formater = new Formater();
        		}
        	}
        }
        return Formater;
	}
	
	/**
	 * retorna {protocol:”pcsmj”, command:”agent-list”, sender:”<IP>”, receptor:”<IP>”}
	 */
	public JSONObject getAgentList(String ip) throws JSONException{
		
		JSONObject agentList = new JSONObject();
		agentList.put("protocol","pcsmj");
		agentList.put("command","agent-list");
		agentList.put("sender", config.getMeuIP());
		agentList.put("receptor",ip);
		return agentList;
	}

	/**
	 * retorna {protocol:”pcsmj”, command:”agent-list-back”, status:”<CODIGO>”, back:”<IP1>,<IP2>,<IPn>”, sender:”<IP>”,receptor:”<IP>”}
	 * Codigo de Status:	200, 400, 401,501
	 */
	public JSONObject getAgentListBack(String ip,int code, LinkedList<String> back) throws JSONException{
		
		JSONObject agentListBack = new JSONObject();
		agentListBack.put("protocol", "pcsmj");
		agentListBack.put("command", "agent-list-back");
		agentListBack.put("status",code);
		
		
		
		
		agentListBack.put("back", back);
		agentListBack.put("sender",config.getMeuIP());
		agentListBack.put("receptor",ip);
		return agentListBack;
	}
	
	/**
	 * retorna {protocol:”pcsmj”, command:”ping”, sender:”<IP>”,receptor:”<IP>”}
	 */
	public JSONObject getPing(String ip) throws JSONException{
		
		JSONObject ping = new JSONObject();		
		ping.put("protocol", "pcsmj");
		ping.put("sender",config.getMeuIP());
		ping.put("command", "ping");
		ping.put("receptor",ip);
		return ping;	
	}
	
	/**
	 * retorna {protocol:”pcsmj”, command:”pong”,status:”<CODIGO>”,  sender:”<IP>”,receptor:”<IP>”}
	 * codigo do status : 100
	 */
	public JSONObject getPong(String ip, Integer codigo) throws JSONException{
		
		JSONObject pong = new JSONObject();
		pong.put("protocol", "pcsmj");
		pong.put("command",	"pong");
		pong.put("status",Integer.toString(codigo));
		pong.put("sender", config.getMeuIP());
		pong.put("receptor", ip);
		return pong;
	}

	/**
	 * 	 {protocol:”pcsmj”, command:”authenticate”, nick_name:”<STRING>”, public_key: ”<STRING>”, ”sender:”<IP>”, receptor:”<IP>”}
	 */
	public JSONObject getAuthenticate(String ip, String nickName, String publicKey) throws JSONException{
		
		JSONObject authenticate = new JSONObject();
		authenticate.put("protocol", "pcsmj");
		authenticate.put("command", "authenticate");
		authenticate.put("nick_name", nickName);
		authenticate.put("public_key", publicKey);
		authenticate.put("sender",config.getMeuIP());
		authenticate.put("receptor", ip);
		return authenticate;		
	}
	
	/**	
	 * retorna {protocol:”pcsmj”, command:”authenticate-back”,status:”<CODIGO>”,”sender:”<IP>”,receptor:”<IP>”}	
	 * Codigo de Status:200,203,400,501
	 */
	public JSONObject getAuthenticateBack(Integer status,String ip) throws JSONException{
		
		JSONObject back = new JSONObject();
		back.put("protocol", "pcsmj");
		back.put("command", "authenticate-back");
		back.put("status", Integer.toString(status));
		back.put("sender",config.getMeuIP());
		back.put("receptor", ip);		
		return back;
	}
	
	/**
	 * retorna {protocol:”pcsmj”, command:”archive-list”, sender:”<IP>”,receptor:”<IP>”}
	 */
	public JSONObject getArchiveList(String ip) throws JSONException{
		
		JSONObject list = new JSONObject();
		list.put("protocol","pcsmj");
		list.put("command","archive-list");
		list.put("sender", config.getMeuIP());
		list.put("receptor", ip);
		return list;
	}
	
	/**
	 * retorna {protocol:”pcsmj”, command:”archive-list-back”, status:”<CODIGO>”, back:”{file:{id:”1”, nome:”file.txt”, size:”200”}},
	 * 				folder:{name:”pasta”, id:”2”},file:{id:”3”, nome:”file1.txt”, size:”100kb”}” sender:”<IP>”,receptor:”<IP>”}
	 * 	Codigo de Status:200, 400,401,501
	 */
	public JSONObject getArchiveListBack(LinkedList<String>archives,Integer status,String ip ) throws JSONException {
		
		JSONObject listBack = new JSONObject();
		JSONObject list = new JSONObject();
		JSONArray aux = new JSONArray();
		Arquivo arq = Arquivo.getArquivo();
		listBack.put("protocol","pcsmj");
		listBack.put("command", "archive-list-back");
		listBack.put("status", Integer.toString(status));
		
		for(int i = 0; i < archives.size(); i++){
			list = new JSONObject();
			list.put("id",Integer.toString(i));
			list.put("name",archives.get(i));
			list.put("size",Integer.toString(arq.getTamanhoPeloId(i)));
			aux.put(i, list);
		}
		listBack.put("back", aux);
		listBack.put("sender", config.getMeuIP());
		listBack.put("receptor", ip);
		return listBack;
	}
	
	/**
	 * retorna {protocol:”pcsmj”, command:”archive-request”, id:”<ID>” sender:”<IP>”,receptor:”<IP>”}
	 */
	public JSONObject getArchiveRequest(Integer id, String ip) throws JSONException {
		
		JSONObject request = new JSONObject();
		request.put("protocol", "pcsmj");
		request.put("command", "archive-request");
		request.put("id", Integer.toString(id));
		request.put("sender", config.getMeuIP());
		request.put("receptor", ip);
		return request;
	}
	
	/**
	 * retorna {protocol:”pcsmj”, command:”archive-request-back”,status:”<CODIGO>”, id:”<ID>”,
	 * 			http_address:”<STRING>”, size:”200”, md5:”<STRING>”, sender:”<IP>”, receptor:”<IP>”}
	 * Codigo de Status:	302, 400,401,404,408,501
	 */
	public JSONObject getArchiveRequestBack(Integer status,Integer id,Integer size, String md5, String adress, String ip) throws JSONException{
		
		JSONObject reqBack = new JSONObject();
		reqBack.put("protocol", "pcsmj");
		reqBack.put("command", "archive-request-back");
		reqBack.put("status",Integer.toString(status));
		reqBack.put("id", Integer.toString(id));
		reqBack.put("md5", md5);
		reqBack.put("http_address", adress);
		reqBack.put("size",Integer.toString(size));
		reqBack.put("sender", config.getMeuIP());
		reqBack.put("receptor",ip);
		return reqBack;
	}
	
	/**
	 * retorna {protocol:”pcsmj”, command:”end-connection”, sender:”<IP>”, receptor:”<IP>”}
	 */
	public JSONObject getEndConnection(String ip) throws JSONException{
		
		JSONObject end = new JSONObject();
		end.put("protocol", "pcsmj");
		end.put("command", "end-connection");
		end.put("sender", config.getMeuIP());
		end.put("receptor", ip);
		return end;
	}
	
	
	/**
	 * {protocol:”pcsmj”, command:”certify”,  nick_name:”<STRING>”, public_key:”<STRING>”, sender:”<IP>”, receptor:”<IP>”}
	 * @throws JSONException 
	 */
	public JSONObject getCertify(String nickName, String publicKey, String ip) throws JSONException{
		JSONObject certify = new JSONObject();
		
		certify.put("protocol","pcsmj");
		certify.put("command", "certify");
		certify.put("nick_name", nickName );
		certify.put("publicKey", publicKey);
		certify.put("sender", config.getMeuIP());
		certify.put("receptor", ip);
		
		return certify;
	}
	
	/**
	 * {protocol:”pcsmj”, command:”certify-back”,  status:”<CODIGO>”, nick_name:”<STRING>”, certify_address:”<STRING>”, sender:”<IP>”, receptor:”<IP>”}
	 *
	 *Codigo de Status:
     *302, 400,401,404,408,501
	 */
	
	public JSONObject getCertifyBack(Integer status, String nickName, String certifyAddress, String ip) throws JSONException{
		JSONObject back = new JSONObject();
		
		back.put("protocol", "pcsmj");
		back.put("command", "certify_back");
		back.put("status", status);
		back.put("nick_name", nickName);
		back.put("certify_address", certifyAddress);
		back.put("sender", config.getMeuIP());
		back.put("receptor", ip);
		
		return back;
	}
	
	
	
	/**
	 * Pega uma string e retonar o JSONObject dela para ser manipulado
	 */
	public JSONObject getJSON(String retorno) throws JSONException {
		JSONObject json = new JSONObject();
			log.severe("Recebeu "+retorno);
			try{
				json = new JSONObject(retorno);
			} catch (JSONException e){
				log.info("Erro: " + e);
				
			}
			return json;
	}
	
	public Comando getComandoByString(String comando){
						
				if (comando.contains("agent-list")){
					return Comando.agent_list;
				}
				else if (comando.contains("agent-list-back")){
					return Comando.agent_list_back;
				}
				else if (comando.contains("ping")){
					return Comando.ping;
				}
				else if (comando.contains("pong")) {		
					return Comando.pong;
				}
				else if (comando.contains("authenticate")){
					return Comando.authenticate;
				}
				else if (comando.contains("authenticate-back")){
					return Comando.authenticate_back;
				}
				else if (comando.contains("archive-list")){
					return Comando.archive_list;
				}
				else if (comando.contains("archive-list-back")){
					return Comando.agent_list_back;
				}
				else if (comando.contains("archive-request")){
					return Comando.archive_request;
				}
				else if (comando.contains("archive-request-back")){
					return Comando.archive_request_back;
				}
				else if (comando.contains("end-connection")){
					return Comando.end_connection;
				}
				else{
					return Comando.none;
					}
		 	}
					
	
	
}
