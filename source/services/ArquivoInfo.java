package services;
public class ArquivoInfo{
	String nome;
	String tamanho;
	String id;
	String md5;
	
	public ArquivoInfo(String id, String nome, String tamanho){
		this.nome = nome;
		this.tamanho = tamanho;
		this.id = id;
	}
	
	public String getId(){
		return this.id;
	}
	
	public String getSize(){
		return this.tamanho;
	}
	
	public String getNome(){
		return this.nome;
	}
	
	public String getMD5(){
		return this.md5;
	}
	
	public void setMD5(String md5){
		this.md5 = md5;
	}
}