package services;
/**
 *  Este é o nenhum que contém todos os comandos 
 *  	possíveis pelo protocolo PCMJ e métodos de
 *  	conversão que axíliam-o
 */
public enum Comando{
	agent_list, agent_list_back, 
	ping, pong, 
	authenticate, authenticate_back,
	archive_list, archive_list_back, 
	archive_request,archive_request_back, 
	end_connection,none;
	
	public static String getStringFromComando(Comando status){
		
		switch(status){
			case agent_list:
				return "agent-list";
			case agent_list_back:
				return "agent-list-back";
			case ping:
				return "ping";
			case pong:
				return "pong";
			case authenticate:
				return "authenticate";
			case authenticate_back:
				return "authenticate-back";
			case archive_list:
				return "archive-list";
			case archive_list_back:
				return "archive-list-back";
			case archive_request:
				return "archive-request";
			case archive_request_back:
				return "archive-request-back";
			case end_connection:
				return "end-connection";
			default:
				return "none";
		}
	}
	
	public static Comando getComandoFromString(String status){
		
		if(status.equals("agent-list")){
			return agent_list;
		}else if(status.equals("agent-list-back")){
			return agent_list_back;
		}else if(status.equals("ping")){
			return ping;
		}else if(status.equals("pong")){
			return pong;
		}else if(status.equals("authenticate")){
			return authenticate;
		}else if(status.equals("authenticate-back")){
			return authenticate_back;
		}else if(status.equals("archive-list")){
			return archive_list;
		}else if(status.equals("archive-list-back")){
			return archive_list_back;
		}else if(status.equals("archive-request")){
			return archive_request;
		}else if(status.equals("archive-request-back")){
			return archive_request_back;
		}else if(status.equals("end-connection")){
			return end_connection;
		}else if(status.equals("none")){
			return none;
		}else{
			return none;
		}
		
	}
	
	public static Comando getComandoBackFromString(String status){
		if(status.equals("agent-list")){
			return agent_list_back;
		}else if(status.equals("agent-list-back")){
			return agent_list_back;
		}else if(status.equals("ping")){
			return pong;
		}else if(status.equals("pong")){
			return pong;
		}else if(status.equals("authenticate")){
			return authenticate_back;
		}else if(status.equals("authenticate-back")){
			return authenticate_back;
		}else if(status.equals("archive-list")){
			return archive_list_back;
		}else if(status.equals("archive-list-back")){
			return archive_list_back;
		}else if(status.equals("archive-request")){
			return archive_request_back;
		}else if(status.equals("archive-request-back")){
			return archive_request_back;
		}else if(status.equals("end-connection")){
			return end_connection;
		}else if(status.equals("none")){
			return none;
		}else{
			return none;
		}
	}
	
	public static Comando getComandoBackFromComando(Comando status){
		
		switch(status){
			case agent_list:
				return agent_list_back;
			case agent_list_back:
				return agent_list_back;
			case ping:
				return pong;
			case pong:
				return pong;
			case authenticate:
				return authenticate_back;
			case authenticate_back:
				return authenticate_back;
			case archive_list:
				return archive_list_back;
			case archive_list_back:
				return archive_list_back;
			case archive_request:
				return archive_request_back;
			case archive_request_back:
				return archive_request_back;
			case end_connection:
				return end_connection;
			default:
				return none;
		}
	}

	public static String getStatusMensagem(Integer status){
		
		switch(status){
			case 100:
				return "Pode continuar";
			case 200:
				return "OK";
			case 203:
				return "Não-Autorizado";
			case 302:
				return "Encontrado";
			case 400:
				return "Requisição inválida";
			case 401:
				return "Acesso não autorizado";
			case 404:
				return "Arquivo não encontrado";
			case 408:
				return "Timeout";
			case 501:
				return "Não implementado";
			default:
				return "Commad NOT found";
		}
	}
}